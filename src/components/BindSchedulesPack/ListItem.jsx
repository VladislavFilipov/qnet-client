import React, { useState } from 'react';
// import axios from 'axios';

import api from '../../api';

export default function ListItem({ pack, schedule, updateTable }) {

    let [showButtonNote, setShowButtonNote] = useState(false);

    const bindQuery = (e) => {


        let li = e.currentTarget.parentNode.parentNode;
        // console.log(li);
        if (li.querySelector('.sub-desc textarea').value === '') {
            setShowButtonNote(true);
            return;
        }


        api.bindSchedulesPack(pack.id, schedule.id, li.querySelector('.sub-desc textarea').value)
            .then((res) => {
                updateTable();
                console.log(res);

            })
            .catch(console.log);

        li.style.display = 'none';

    }

    return (
        <li key={`${pack.id}-${schedule.id}`} >
            <span className="package">{pack.title} : {pack.desc}</span>
            <span className="schedule">{schedule.desc}</span>
            <div className="sub-desc">
                <div>Заполните описание новой подписки</div>
                <textarea placeholder="Описание подписки..."></textarea>
            </div>
            <div className="bind-button">
                <span className="close" onClick={(e) => (
                    e.currentTarget.parentNode.parentNode.parentNode.style.display = 'none'
                )}>+</span>
                <button onClick={bindQuery} >
                    <span>Связать</span>
                    {showButtonNote && <div className="tooltip tooltip-top">
                        Заполните описание!
                    </div>}
                </button>

            </div>
        </li>
    );

};