import React, { useState, useEffect } from 'react';
import SearchBar from '../SearchBar/SearchBar';
import ListItem from './ListItem';
import api from '../../api';

export default function BindUsersGroups() {

    let [packages, setPackages] = useState([]);
    let [chosenPackages, setChosenPackages] = useState([]);
    let [schedules, setSchedules] = useState([]);
    let [chosenSchedules, setChosenSchedules] = useState([]);
    let [table, setTable] = useState([]);
    let [closed, setClosed] = useState('-closed');
    let [searchFilter, setSearchFilter] = useState('');

    useEffect(() => {
        api.getPackages().then(res => setPackages(res.data)).catch(console.log);
        api.getSchedules().then(res => setSchedules(res.data)).catch(console.log);
        api.getSubscriptions().then(res => { setTable(res.data); console.log(res.data) }).catch(console.log);
    }, []);

    let chosenArray = [];

    chosenSchedules.forEach((schedule) => {
        chosenPackages.forEach((pack) => {
            chosenArray.push(
                <ListItem
                    key={`${pack.id}-${schedule.id}`}
                    pack={pack}
                    schedule={schedule}
                    updateTable={() => api.getSubscriptions().then(res => setTable(res.data)).catch(console.log)}
                />
            );
        })
    });

    const filteredTable = table.length === 0 ? table : table.filter(item => item.title.toLowerCase().includes(searchFilter.toLowerCase()) || item.desc1.toLowerCase().includes(searchFilter.toLowerCase()) || item.desc2.toLowerCase().includes(searchFilter.toLowerCase()));

    return (

        <div className="BindSchedulesPack">
            <SearchBar
                data={packages}
                type="packs" sort1="title" sort2="" placeholder="Выберите пакеты..."
                chosen={chosenPackages}
                returnChosen={chosen => setChosenPackages(chosen)}
            />

            <SearchBar
                data={schedules}
                type="schedules" sort1="desc" sort2="" placeholder="Выберите расписания..."
                chosen={chosenSchedules}
                returnChosen={chosen => setChosenSchedules(chosen)}
            />

            {chosenPackages.length !== 0 &&
                chosenSchedules.length !== 0 &&
                <ul className="bind-list">
                    <li>
                        <span className="package">Пакет</span>
                        <span className="schedule">Расписание</span>

                    </li>
                    {chosenArray}
                </ul>
            }

            <div className="table-container">
                <div
                    className="table-header"

                >
                    <span>Таблица связей расписаний и пакетов</span>
                    <input type="text" value={searchFilter} placeholder="Поиск" onChange={e => setSearchFilter(e.currentTarget.value)} />
                    <img src={`http://${process.env.REACT_APP_URL}/static/ar-r.svg`} alt="" className={closed === '-closed' ? '' : 'up'} onClick={() => setClosed(closed === '-closed' ? '' : '-closed')} />
                </div>

                <div className={`table-body ${closed}`}>
                    <table className="tableView">
                        <thead>
                            <tr>
                                <th>Название пакета</th>
                                <th>Описание расписания</th>
                                <th>Описание подписки</th>
                            </tr>
                        </thead>
                        <tbody>
                            {filteredTable.map((element, i) =>
                                <tr key={i} className="table__row">
                                    <td className="row__item">{element.title}</td>
                                    <td className="row__item">{element.desc1}</td>
                                    <td className="row__item">{element.desc2}</td>
                                </tr>
                            )}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    );
}
