import React, { useState } from 'react';

function getStr(type, item) {
    if (type === 'users') return item.login;
    else if (type === 'groups') return item.title;
    else if (type === 'subs') return item.desc;
    else if (type === 'courses') return item.title;
    else if (type === 'packs') return item.title + ' : ' + item.desc;
    else if (type === 'schedules') return item.desc;
}

function dataSorting(data, sorting, sort1, sort2) {
    if (sorting.by === 'name') {
        data.sort((a, b) => {
            if (a[sort1] === null) return -1;
            if (b[sort1] === null) return 1;

            if (sorting.order === 'ascending') {
                if (a[sort1].toLowerCase() > b[sort1].toLowerCase()) return 1;
                else if (a[sort1].toLowerCase() < b[sort1].toLowerCase()) return -1;
                else return 0;
            } else {
                if (a[sort1].toLowerCase() < b[sort1].toLowerCase()) return 1;
                else if (a[sort1].toLowerCase() > b[sort1].toLowerCase()) return -1;
                else return 0;
            }
        });
    } else {
        if (sorting.order === 'ascending')
            data.sort((a, b) => a[sort2] - b[sort2]);
        else
            data.sort((a, b) => b[sort2] - a[sort2]);
    }

    return data;
}

export default function SearchBar({ type, data, sort1, sort2, placeholder, chosen, returnChosen }) {

    let [inputFocused, setInputFocused] = useState(false);
    let [searchFilter, setSearchFilter] = useState('');
    let [sorting, setSorting] = useState({
        by: 'name',
        order: 'ascending'
    });

    const inputClickHandler = (e) => {
        const list = e.target.classList;
        if (!list.contains(`${type}-list-item`)
            && !list.contains(`sorting-type`)
            && !list.contains(`${type}-input-field`)
        ) {
            document.removeEventListener('click', inputClickHandler);
            setInputFocused(false);
        }
    }

    const onInputCkick = () => {
        setInputFocused(true);
        document.addEventListener('click', inputClickHandler);
    }

    const onInputChange = (e) => {
        setInputFocused(true);
        setSearchFilter(e.target.value);
    }

    const onListItemClick = (e) => {

        e.target.parentNode.parentNode.querySelector(`.${type}-input-field`).focus();

        if (chosen.find(item => item.id === e.target.dataset.id)) return;

        returnChosen([...chosen, {
            id: e.target.dataset.id,
            title: e.target.dataset.title,
            desc: e.target.dataset.desc,
            login: e.target.dataset.login
        }]);
    }

    const listItemClose = (e) => {
        let li = e.target.parentNode;
        returnChosen(chosen.filter(item => item.id !== li.dataset.id));
    }

    let searchList = [];
    let searchData = dataSorting(data.slice(0), sorting, sort1, sort2);

    searchData.forEach((item, i) => {

        const str = getStr(type, item);
        if (str) {
            if (searchFilter.toLowerCase() && !str.toLowerCase().includes(searchFilter))
                return;
            else
                searchList.push(<li
                    key={i}
                    className={`${type}-list-item`}
                    onClick={onListItemClick}
                    data-id={item.id}
                    data-login={item.login ? item.login : ''}
                    data-title={item.title ? item.title : ''}
                    data-desc={item.desc ? item.desc : ''}
                >{str}</li>);
        }
    });

    const name = type === 'users' ? "имени" : "названию";
    const reg = type === 'users' ? "дате регистрации" : (type === 'courses' ? "порядку" : "");

    return (
        <div className={`${type}`} >

            <div className={`${type}-select`}>

                <div className="select-sorting">
                    <span>Сортировать по</span>
                    {name &&
                        <span
                            className={`sorting-type ${sorting.by === 'name' && 'active'}`}
                            onClick={() => {
                                setSorting(prev => ({ by: 'name', order: prev.order }));
                                onInputCkick();
                            }}
                        >{name}</span>}
                    {reg &&
                        <span
                            className={`sorting-type ${sorting.by === 'reg' && 'active'}`}
                            onClick={() => {
                                setSorting(prev => ({ by: 'reg', order: prev.order }));
                                onInputCkick();
                            }}
                        >{reg}</span>}
                    <img
                        src={`http://${process.env.REACT_APP_URL}/static/order-white.svg`}
                        alt="возрастанию / убыванию"
                        title="возрастанию / убыванию"
                        className={`sorting-type ${sorting.order === 'descending' && 'descending'}`}
                        onClick={() => {
                            setSorting(prev => ({ by: prev.by, order: prev.order === 'ascending' ? 'descending' : 'ascending' }))
                            onInputCkick();
                        }}
                    />
                </div>

                <input
                    type="text"
                    placeholder={placeholder}
                    onClick={onInputCkick}
                    onInput={onInputChange}
                    className={`${type}-input-field q-input`}

                />

                {inputFocused && <ul className={`${type}-list-container`}>
                    {searchList}
                </ul>}
            </div>


            <ul className={`${type}-chosen`}>
                {
                    chosen.map((item, i) => (
                        <li
                            key={i}
                            data-id={item.id}
                            className="  -chosen"
                        >
                            {getStr(type, item)}
                            <span className="btn-close" onClick={listItemClose}>
                                +
                            </span>
                        </li>
                    ))
                }
            </ul>
        </div>
    );
};