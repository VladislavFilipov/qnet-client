import React from 'react';
import { Link, useLocation, Route, useHistory } from 'react-router-dom';
import { reactLocalStorage } from 'reactjs-localstorage';
// import axios from 'axios';

import BindUsersGroups from '../BindUsersGroups/BindUsersGroups';
import BindGroupsSubs from '../BindGroupsSubs/BindGroupsSubs';
// import BindSchedules from './BindSchedules/BindSchedules';
import CreateSchedule from '../Schedules/CreateSchedule';
import PackagesAdd from '../PackagesAdd/PackagesAdd'
import BindPackCourses from '../BindPackCourses/BindPackCourses';
import BindSchedulesPack from '../BindSchedulesPack/BindSchedulesPack';
import GeneralTable from '../GeneralTable/GeneralTable';


const url = process.env.REACT_APP_URL;
const port = process.env.REACT_APP_PORT;

export const actions = [
    { name: "Общая таблица", path: "/actions", component: <GeneralTable />, iconFile: "home-icon.svg" },
    { name: "Связать пользователей и группы", path: "/actions/bind-users-groups", component: <BindUsersGroups />, iconFile: "bind-icon.svg" },
    { name: "Связать подписки и группы", path: "/actions/bind-subs-groups", component: <BindGroupsSubs />, iconFile: "bind-icon.svg" },
    { name: "Создать расписание", path: "/actions/create-schedule", component: <CreateSchedule url={url} port={port} />, iconFile: "schedule-icon.svg" },
    { name: "Добавить пакет", path: "/actions/add-pack", component: <PackagesAdd />, iconFile: "package-icon.svg" },
    { name: "Связать курсы и пакеты", path: "/actions/bind-courses-packs", component: <BindPackCourses />, iconFile: "bind-icon.svg" },
    { name: "Связать пакеты и расписания", path: "/actions/bind-packs-schedules", component: <BindSchedulesPack />, iconFile: "bind-icon.svg" },
]

export default function Main() {
    const location = useLocation().pathname;
    const history = useHistory();

    return (
        <div className="Main">
            <div className="header">
                <div className="logo">
                    <img src={`http://${process.env.REACT_APP_URL}/static/logo.png`} alt="" />
                </div>
                <div className="chosen-action">{actions.find(action => action.path === location).name}</div>
                <div>
                    {reactLocalStorage.get('user')}
                    <img src={`http://${process.env.REACT_APP_URL}/static/sign-out-alt-solid.svg`} alt="" onClick={() => {
                        reactLocalStorage.clear();
                        history.push('/auth');
                    }} />
                </div>
            </div>
            <ul className="side-bar">
                {
                    actions.map((action, i) => (
                        <Link key={i} to={action.path} className={`list-item ${action.path === location ? 'active' : ''}`} >
                            <img src={`http://${process.env.REACT_APP_URL}/static/${action.iconFile}`} alt="" />
                            <div>{action.name}</div>
                            <img src={`http://${process.env.REACT_APP_URL}/static/ar-r.svg`} alt="" className={`${action.path === location ? 'left' : ''}`} />
                        </Link>
                    ))
                }
            </ul>
            <div className="block-view">
                {actions.map((action, i) => (
                    <Route key={i} exact path={action.path}>{action.component}</Route>
                ))}
            </div>
        </div>
    )
};


