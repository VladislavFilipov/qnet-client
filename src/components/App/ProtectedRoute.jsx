import React, { useState, useEffect } from "react";
import { Route, Redirect } from "react-router-dom";

import api from '../../api';

export const ProtectedRoute = ({
    component: Component,
    ...rest
}) => {

    let [isAuthed, setIsAuthed] = useState(null);

    useEffect(() => {
        api.isAuthed()
            .then(() => setIsAuthed(200), () => setIsAuthed(401));
    }, []);

    if (isAuthed === null) {
        return <Route
            {...rest}
            component={
                () => <div className="Loading">
                    <img src={`http://${process.env.REACT_APP_URL}/static/spinner-solid.svg`} alt="" />
                </div>
            }
        />
    }

    return (
        <Route
            {...rest}
            render={props => {
                if (isAuthed === 200) {
                    return <Component {...props} />;
                } else if (isAuthed) {
                    return <Redirect to={"/auth"} />;
                }
            }}
        />
    );
};
