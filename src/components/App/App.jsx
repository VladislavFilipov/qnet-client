import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import Main from './Main';
import Auth from '../Auth/Auth';

import './App.scss';
import { ProtectedRoute } from './ProtectedRoute';

export default function App() {

    return (
        <div className="App">
            <Switch>
                <Route path="/auth" component={Auth} />
                <ProtectedRoute path="/actions" component={Main} />
                <Route path="/" component={() => <Redirect to="/actions" />} />
                <Route path="*" component={() => "404 NOT FOUND"} />
            </Switch>
        </div>
    )
};