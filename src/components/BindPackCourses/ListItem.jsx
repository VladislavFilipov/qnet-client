import React, { useState, useEffect } from 'react';
import Works from '../Works/Works.jsx';
import api from '../../api';

export default function ListItem({ course, pack, updateTable }) {

    let [works, setWorks] = useState([]);
    let [chosenWorks, setChosenWorks] = useState([]);

    useEffect(() => {
        api.getWorks().then(res => setWorks(res.data)).catch(console.log);
    }, []);

    const bindQuery = (e) => {


        let li = e.currentTarget.parentNode.parentNode;
        let excluded_works = "";
        chosenWorks.forEach((element) => {
            excluded_works += element.id + ", ";
        });
        excluded_works = excluded_works.slice(0, -2);

        api.bindPackCourses(pack.id, course.id, excluded_works)
            .then(() => {
                updateTable();
                setChosenWorks([]);
            })
            .catch(console.log);

        li.style.display = 'none';

    }

    return (
        <li key={`${pack.id}-${course.id}`} data-package={pack.id} data-course={course.id} >
            <span className="sub">{pack.title}</span>
            <span className="group">{course.title}</span>

            <Works
                works={works}
                flag={chosenWorks.length === 0}
                returnChosen={chosen => setChosenWorks(chosen)}
            />

            <div className="bind-button">
                <span className="close" onClick={(e) => (
                    e.currentTarget.parentNode.parentNode.parentNode.style.display = 'none'
                )}>+</span>
                <button onClick={bindQuery} >
                    <span>Связать</span>
                </button>
            </div>
        </li>
    );
}