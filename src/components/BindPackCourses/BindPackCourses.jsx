import React, { useState, useEffect } from 'react';

import SearchBar from '../SearchBar/SearchBar';
import ListItem from './ListItem';

import api from '../../api';

export default function BindPackCourses() {

    let [packages, setPackages] = useState([]);
    let [chosenPackages, setChosenPackages] = useState([]);
    let [courses, setCourses] = useState([]);
    let [chosenCourses, setChosenCourses] = useState([]);
    let [table, setTable] = useState([]);
    let [closed, setClosed] = useState('-closed');
    let [searchFilter, setSearchFilter] = useState('');

    useEffect(() => {
        api.getPackages().then(res => setPackages(res.data)).catch(console.log);
        api.getCourses().then(res => setCourses(res.data)).catch(console.log);
        api.getCoursesPack().then(res => setTable(res.data)).catch(console.log);
    }, []);

    let chosenArray = [];

    chosenCourses.forEach((_course, c) => {
        chosenPackages.forEach((_package, p) => {
            chosenArray.push(
                <ListItem
                    key={`${c}-${p}`}
                    course={_course}
                    pack={_package}
                    updateTable={() => api.getCoursesPack().then(res => setTable(res.data)).catch(console.log)}
                />
            );
        })
    });

    const filteredTable = table.length === 0 ? table : table.filter(item => item.title1.toLowerCase().includes(searchFilter.toLowerCase()) || item.title2.toLowerCase().includes(searchFilter.toLowerCase()));

    return (
        <div className="BindPackCourses">
            <SearchBar
                data={packages}
                type="packs" sort1="title" sort2="" placeholder="Выберите пакеты..."
                chosen={chosenPackages}
                returnChosen={chosen => setChosenPackages(chosen)}
            />

            <SearchBar
                data={courses}
                type="courses" sort1="title" sort2="numb" placeholder="Выберите курсы..."
                chosen={chosenCourses}
                returnChosen={chosen => setChosenCourses(chosen)}
            />

            {chosenCourses.length !== 0 &&
                chosenPackages.length !== 0 &&
                <ul className="bind-list">
                    <li>
                        <span className="sub">Пакет</span>
                        <span className="group">Курс</span>

                    </li>
                    {chosenArray}
                </ul>
            }

            <div className="table-container">
                <div
                    className="table-header"
                >
                    <span>Таблица связей пакетов и курсов</span>
                    <input type="text" value={searchFilter} placeholder="Поиск" onChange={e => setSearchFilter(e.currentTarget.value)} />
                    <img src={`http://${process.env.REACT_APP_URL}/static/ar-r.svg`} alt="" className={closed === '-closed' ? '' : 'up'} onClick={() => setClosed(closed === '-closed' ? '' : '-closed')} />
                </div>

                <div className={`table-body ${closed}`}>
                    <table className="tableView">
                        <thead>
                            <tr>
                                <th>Название курса</th>
                                <th>Название пакета</th>
                                <th>Исключенные работы</th>
                            </tr>
                        </thead>
                        <tbody>
                            {filteredTable.map((element, i) =>
                                <tr key={i} className="table__row">
                                    <td className="row__item">{element.title1}</td>
                                    <td className="row__item">{element.title2}</td>
                                    <td className="row__item">{element.excluded_works}</td>
                                </tr>
                            )}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
}

