import React, { useState, useEffect } from 'react';

// import { inputClickHandler, onInputCkick, onInputChange, onListItemClick, listItemClose } from './../searchLineTemplateFuncs';

export default function Works({ works, returnChosen, flag }) {
    let [inputFocused, setInputFocused] = useState(false);
    let [searchFilter, setSearchFilter] = useState('');
    let [chosen, setChosen] = useState([]);

    useEffect(() => {
        if (flag) setChosen([]);
    }, [flag]);

    useEffect(() => returnChosen(chosen), [chosen, returnChosen]);

    const inputClickHandler = (e) => {
        if (e.target.className !== `works-list-item`) {
            document.removeEventListener('click', inputClickHandler);
            setInputFocused(false);
        }
    }

    const onInputCkick = () => {
        setInputFocused(true);
        document.addEventListener('click', inputClickHandler);
    }

    const onInputChange = (e) => {
        setInputFocused(true);
        setSearchFilter(e.target.value);
    }

    const onListItemClick = (e) => {
        e.target.parentNode.parentNode.querySelector(`.works-input-field`).focus();

        setChosen([...chosen, {
            id: e.target.dataset.id,
            title: e.target.dataset.title,
            desc: e.target.dataset.desc
        }]);
    }

    const listItemClose = (e) => {
        let li = e.target.parentNode.parentNode;

        let tempChosen = chosen.slice(0);

        tempChosen.forEach((item, i) => {
            if (item.id === li.dataset.id) {
                tempChosen.splice(i, 1);
            }
        });

        setChosen(tempChosen);
    }

    let worksList = [];
    works.forEach((work, i) => {
        if (work.title) {
            if (searchFilter.toLowerCase() && !work.title.toLowerCase().includes(searchFilter))
                return;
            else
                worksList.push(<li
                    key={i}
                    className="works-list-item"
                    onClick={onListItemClick}
                    data-id={work.id}
                    data-title={work.title}
                >{work.title}</li>);
        }
    });

    return (
        <div className="works" >
            Исключить работы: &nbsp;&nbsp;
            <ul className="works-chosen">
                {
                    chosen.map((work, i) => (
                        <li
                            key={i}
                            data-id={work.id}
                        >
                            {work.title}
                            <span className="q-icon">
                                <i className="fa fa-close" onClick={listItemClose}></i>
                            </span>
                        </li>
                    ))
                }
            </ul>

            <div className="works-select">
                <input
                    type="text"
                    placeholder="Выберите работы, которые необходимо исключить..."
                    onClick={onInputCkick}
                    onInput={onInputChange}
                    className="works-input-field"
                />

                {inputFocused && <ul className="works-list-container">
                    {worksList}
                </ul>}
            </div>

        </div>
    );
};