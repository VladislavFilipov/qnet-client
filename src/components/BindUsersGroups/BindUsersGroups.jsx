import React, { useState, useEffect } from 'react';
import SearchBar from '../SearchBar/SearchBar';
import api from '../../api';
import './BindUsersGroups.scss';

export default function BindUsersGroups() {

    let [users, setUsers] = useState([]);
    let [chosenUsers, setChosenUsers] = useState([]);
    let [groups, setGroups] = useState([]);
    let [chosenGroups, setChosenGroups] = useState([]);
    let [table, setTable] = useState([]);
    let [closed, setClosed] = useState('-closed');
    let [searchFilter, setSearchFilter] = useState('');

    useEffect(() => {
        api.getUsers().then(res => setUsers(res.data)).catch(console.log);
        api.getGroups().then(res => setGroups(res.data)).catch(console.log);
        api.getUsersBind().then(res => setTable(res.data)).catch(console.log);
    }, []);

    const bindQuery = () => {
        api.bindUserGroup(chosenUsers, chosenGroups)
            .then(() => {
                api.getUsersBind().then(res => { setTable(res.data); console.log(1.5) }).catch(console.log);

                setChosenUsers([]);
                setChosenGroups([]);
            })
            .catch(console.log);
    }

    const filteredTable = table.length === 0 ? table : table.filter(item => item.login && (item.login.toLowerCase().includes(searchFilter.toLowerCase()) || item.title.toLowerCase().includes(searchFilter.toLowerCase())));

    return (
        <div className="BindUsersGroups">

            <SearchBar
                data={users}
                type="users" sort1="login" sort2="registered" placeholder="Выберите пользователей..."
                chosen={chosenUsers}
                returnChosen={chosen => setChosenUsers(chosen)}
            />

            <div className="bind-button">
                <button
                    className={` ${chosenGroups.length === 0 || chosenUsers.length === 0 ? '-disabled' : ''}`}
                    onClick={bindQuery}
                >
                    Связать
                </button>
            </div>

            <SearchBar
                data={groups}
                type="groups" sort1="title" sort2="" placeholder="Выберите группы..."
                chosen={chosenGroups}
                returnChosen={chosen => setChosenGroups(chosen)}
            />

            <div className="table-container">
                <div
                    className="table-header"

                >
                    <div>Таблица связей пользователей и групп</div>
                    <input type="text" value={searchFilter} placeholder="Поиск" onChange={e => setSearchFilter(e.currentTarget.value)} />
                    <img src={`http://${process.env.REACT_APP_URL}/static/ar-r.svg`} alt="" className={closed === '-closed' ? '' : 'up'} onClick={() => setClosed(closed === '-closed' ? '' : '-closed')} />
                </div>

                <div className={`table-body ${closed}`}>
                    <table className="tableView">
                        <thead>
                            <tr>
                                <th>Имя пользователя</th>
                                <th>Группа</th>
                            </tr>
                        </thead>
                        <tbody>
                            {filteredTable.map((element, i) =>
                                <tr key={i} className="table__row">
                                    <td className="row__item">{element.login}</td>
                                    <td className="row__item">{element.title}</td>
                                </tr>
                            )}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
}
