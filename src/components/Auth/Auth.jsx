import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';

import api from '../../api';

import './Auth.scss';

export default function Auth() {

    let [message, setMessage] = useState('');

    const history = useHistory();

    const formOnSubmit = e => {
        e.preventDefault();
        const form = e.currentTarget;

        api.login(form.elements.login.value, form.elements.psw.value)
            .then(res => {
                if (res === true) {
                    history.push('/actions');
                } else {
                    setMessage('Не удалось войти.')
                }
            });
    }

    return (
        <div className="Auth">
            <div>Войти в административный сервис</div>
            <form onSubmit={formOnSubmit}>
                <input name="login" type="text" placeholder="Логин" />
                <input name="psw" type="password" placeholder="Пароль" />
                <div>
                    <button type="submit">Войти</button>
                </div>
                <div className="Auth__message">{message}</div>
            </form>

        </div>
    )
};