import React, { useState } from 'react';

import api from '../../api';

export default function ListItem({ sub, group, updateTable }) {

    let [showButtonNote, setShowButtonNote] = useState(false);

    const bindQuery = (e) => {
        let li = e.currentTarget.parentNode.parentNode;

        if (li.querySelector('.start-date').value === '' || li.querySelector('.start-time').value === '') {
            setShowButtonNote(true);
            return;
        }

        let date = Date.parse(li.querySelector('.start-date').value + 'T' + li.querySelector('.start-time').value + ':00') / 1000;

        api.bindGroupSub(group.id, sub.id, date)
            .then(() => {
                updateTable();
            })
            .catch(console.log);

        li.style.display = 'none';

    }

    return (
        <li key={`${group.id}-${sub.id}`} >
            <div className="sub">{sub.desc}</div>
            <div className="group">{group.title}</div>
            <div className="datetime">
                Укажите время начала подписки&nbsp;&nbsp;
                <input type="date" className="start-date" />&nbsp;
                <input type="time" className="start-time" />
            </div>
            <div className="bind-button">
                <span className="close" onClick={(e) => (
                    e.currentTarget.parentNode.parentNode.parentNode.style.display = 'none'
                )}>+</span>
                <button onClick={bindQuery} >
                    <span>Связать</span>
                    {showButtonNote && <div className="tooltip">
                        Заполните все поля!
                        </div>}
                </button>

            </div>
        </li>
    );
}