import React, { useState, useEffect } from 'react';

import SearchBar from '../SearchBar/SearchBar';
import ListItem from './ListItem';

import api from '../../api';

const timeConverter = (timestamp) => {
    var a = new Date(timestamp * 1000);
    var months = ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var date = a.getDate();
    var hour = a.getHours();
    var min = "0" + a.getMinutes();
    var sec = "0" + a.getSeconds();
    var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min.substr(-2) + ':' + sec.substr(-2);
    return time;
}

export default function BindUsersGroups() {

    let [subs, setSubs] = useState([]);
    let [chosenSubs, setChosenSubs] = useState([]);
    let [groups, setGroups] = useState([]);
    let [chosenGroups, setChosenGroups] = useState([]);
    let [table, setTable] = useState([]);
    let [closed, setClosed] = useState('-closed');
    let [searchFilter, setSearchFilter] = useState('');

    useEffect(() => {
        api.getSubs().then(res => setSubs(res.data)).catch(console.log);
        api.getGroups().then(res => setGroups(res.data)).catch(console.log);
        api.getGroupsSubscriptions().then(res => setTable(res.data)).catch(console.log);
    }, []);


    let chosenArray = [];

    chosenSubs.forEach((sub, s) => {
        chosenGroups.forEach((group, g) => {
            chosenArray.push(
                <ListItem
                    key={`${s}-${g}`}
                    group={group}
                    sub={sub}
                    updateTable={() => api.getGroupsSubscriptions().then(res => setTable(res.data)).catch(console.log)}
                />
            );
        })
    });

    const filteredTable = table.length === 0 ? table : table.filter(item => item.desc && (item.desc.toLowerCase().includes(searchFilter.toLowerCase()) || item.title.toLowerCase().includes(searchFilter.toLowerCase())));

    return (
        <div className="BindGroupsSubs">
            <SearchBar
                data={subs}
                type="subs" sort1="desc" sort2="" placeholder="Выберите подписки..."
                chosen={chosenSubs}
                returnChosen={chosen => setChosenSubs(chosen)}
            />

            <SearchBar
                data={groups}
                type="groups" sort1="title" sort2="" placeholder="Выберите группы..."
                chosen={chosenGroups}
                returnChosen={chosen => setChosenGroups(chosen)}
            />

            {chosenSubs.length !== 0 &&
                chosenGroups.length !== 0 &&
                <ul className="bind-list ">
                    <li>
                        <span className="sub">Подписка</span>
                        <span className="group">Группа</span>

                    </li>
                    {chosenArray}
                </ul>
            }


            <div className="table-container">
                <div className="table-header" >
                    <span>Таблица связей групп и подписок</span>
                    <input type="text" value={searchFilter} placeholder="Поиск" onChange={e => setSearchFilter(e.currentTarget.value)} />
                    <img src={`http://${process.env.REACT_APP_URL}/static/ar-r.svg`} alt="" className={closed === '-closed' ? '' : 'up'} onClick={() => setClosed(closed === '-closed' ? '' : '-closed')} />
                </div>

                <div className={`table-body ${closed}`}>
                    <table className="tableView">
                        <thead>
                            <tr>
                                <th>Группа</th>
                                <th>Подписка</th>
                                <th>Дата подписки</th>
                            </tr>
                        </thead>
                        <tbody>
                            {filteredTable.map((element, i) =>
                                <tr key={i} className="table__row">
                                    <td className="row__item">{element.title}</td>
                                    <td className="row__item">{element.desc}</td>
                                    <td className="row__item">{timeConverter(element.date)}</td>
                                </tr>
                            )}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
}

