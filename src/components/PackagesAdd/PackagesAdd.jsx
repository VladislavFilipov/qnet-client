import React, { useState, useEffect } from 'react';
import './PackagesAdd.scss';
import api from '../../api';

export default function PackagesAdd() {
    let [closed, setClosed] = useState('-closed');
    let [showButtonNote, setShowButtonNote] = useState(false);
    let [table, setTable] = useState([]);
    let [searchFilter, setSearchFilter] = useState('');

    useEffect(() => {
        api.getPackages().then(res => { console.log(res.data); setTable(res.data) }).catch(console.log);
    }, []);

    const addData = () => {
        let pack = {
            title: document.querySelector('.PackagesAdd .title').value,
            desc: document.querySelector('.PackagesAdd .description').value,
        }

        if (pack.title === '' || pack.desc === '') {
            setShowButtonNote(true);
            return;
        }

        api.addPackage(pack)
            .then((res) => {
                console.log(res);
                api.getPackages().then(res => setTable(res.data)).catch(console.log);
                setShowButtonNote(false);

                document.querySelector('.PackagesAdd .title').value = '';
                document.querySelector('.PackagesAdd .description').value = '';

            })
            .catch(console.log);
    }

    const filteredTable = table.length === 0 ? table : table.filter(item => item.title.toLowerCase().includes(searchFilter.toLowerCase()));

    return (
        <div className="PackagesAdd">
            <div className="package-input-fields">
                <div className="note">Введите данные нового пакета</div>
                <input type="text" className="title" placeholder="Введите название..." />

                <textarea className="description" placeholder="Введите описание..."></textarea>
            </div>

            <div className="add-button">
                <button onClick={addData}>Добавить пакет</button>
                {showButtonNote &&
                    <div className="tooltip">
                        Заполните все поля!
                    </div>}
            </div>

            <div className="table-container">
                <div
                    className="table-header"

                >
                    <span>Таблица пакетов</span>
                    <input type="text" value={searchFilter} placeholder="Поиск" onChange={e => setSearchFilter(e.currentTarget.value)} />
                    <img src={`http://${process.env.REACT_APP_URL}/static/ar-r.svg`} alt="" className={closed === '-closed' ? '' : 'up'} onClick={() => setClosed(closed === '-closed' ? '' : '-closed')} />
                </div>

                <div className={`table-body ${closed}`}>
                    <table className="tableView">
                        <thead>
                            <tr>
                                <th>Название</th>
                                <th>Описание</th>
                            </tr>
                        </thead>
                        <tbody>
                            {filteredTable.map((element, i) =>
                                (<tr key={i} className="table__row">
                                    <td className="row__item">{element.title}</td>
                                    <td className="row__item">{element.desc}</td>
                                </tr>)
                            )}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
};
