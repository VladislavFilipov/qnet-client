import React, { useState, useEffect } from 'react';
import api from '../../api';
import './CreateSchedule.scss';

export default function CreateSchedule() {
    let [showButtonNote, setShowButtonNote] = useState(false);
    let [table, setTable] = useState([]);
    let [closed, setClosed] = useState('-closed');
    let [isCreate, setIsCreate] = useState('create');
    let [chosenSchedule, setChosenSchedule] = useState(-1);
    let [searchFilter, setSearchFilter] = useState('');

    useEffect(() => {
        api.getSchedules().then(res => setTable(res.data)).catch(console.log);
    }, []);

    const validateTimes = (schedule) => {

        let res = true;

        if (schedule.weekdays.length === 0 ||
            schedule.times.length === 0
        ) res = false;

        const re = /[0-9]{1,2}:[0-9]{2}:[0-9]{2}/;
        schedule.times.forEach(time => {
            if (!re.test(time.tbeg) || !re.test(time.tend)) res = false;
            if (time.tbeg >= time.tend) res = false;
        })

        return res;
    }

    const addSchedule = (form, schedule) => {
        schedule.duration = form.elements.duration.value;
        schedule.description = form.elements.description.value;

        if (!validateTimes(schedule) ||
            !schedule.duration ||
            !schedule.description
        ) {
            setShowButtonNote(true);
            return false;
        }

        api.addSchedule(schedule)
            .then((res) => {
                console.log(res);
                api.getSchedules().then(res => setTable(res.data)).catch(console.log);
                form.reset();
            })
            .catch(console.log);
    }

    const updateSchedule = (form, schedule) => {
        const chosen = table.find(item => item.id === chosenSchedule)
        schedule.duration = chosen.duration;
        schedule.description = chosen.description;
        schedule.id = chosenSchedule;

        if (!validateTimes(schedule)) {
            setShowButtonNote(true);
            return;
        }

        api.updateSchedule(schedule)
            .then((res) => {
                console.log(res);
                api.getSchedules().then(res => setTable(res.data)).catch(console.log);
                form.reset();
            })
            .catch(console.log);
    }

    const onFormSubmit = (e) => {
        e.preventDefault();

        const form = e.currentTarget;

        let schedule = {
            weekdays: [],
            times: [],
            duration: null,
            description: ''
        };

        form.elements.weekdays.forEach((weekday) => {
            if (weekday.checked) schedule.weekdays.push(weekday.dataset.weekday);
        });

        form.elements.times.forEach((time) => {
            if (time.checked) schedule.times.push({ tbeg: time.dataset.tbeg, tend: time.dataset.tend });
        });

        if (isCreate)
            return addSchedule(form, schedule);
        else
            return updateSchedule(form, schedule);

    }

    const filteredTable = table.length === 0 ? table : table.filter(item => item.desc.toLowerCase().includes(searchFilter.toLowerCase()));

    return (
        <div className="CreateSchedule">

            <form onSubmit={onFormSubmit}>
                <div>
                    <div className="schedule-weekdays">
                        <div className="subblock-title">Дни недели</div>
                        <label><input type="checkbox" name="weekdays" className="weekdays" data-weekday="Monday" />Понедельник</label>
                        <label><input type="checkbox" name="weekdays" className="weekdays" data-weekday="Tuesday" />Вторник</label>
                        <label><input type="checkbox" name="weekdays" className="weekdays" data-weekday="Wednesday" />Среда</label>
                        <label><input type="checkbox" name="weekdays" className="weekdays" data-weekday="Thursday" />Четверг</label>
                        <label><input type="checkbox" name="weekdays" className="weekdays" data-weekday="Friday" />Пятница</label>
                        <label><input type="checkbox" name="weekdays" className="weekdays" data-weekday="Saturday" />Суббота</label>
                        <label><input type="checkbox" name="weekdays" className="weekdays" data-weekday="Sunday" />Воскресенье</label>
                    </div>

                    <div className="schedule-lessons">
                        <div className="subblock-title">Время</div>
                        <label><input type="checkbox" name="times" data-tbeg="09:30:00" data-tend="10:35:00" />1 пара: 09:30:00 - 10:35:00</label>
                        <label><input type="checkbox" name="times" data-tbeg="10:45:00" data-tend="12:20:00" />2 пара: 10:45:00 - 12:20:00</label>
                        <label><input type="checkbox" name="times" data-tbeg="13:00:00" data-tend="14:35:00" />3 пара: 13:00:00 - 14:35:00</label>
                        <label><input type="checkbox" name="times" data-tbeg="14:45:00" data-tend="16:20:00" />4 пара: 14:45:00 - 16:20:00</label>
                        <label><input type="checkbox" name="times" data-tbeg="16:30:00" data-tend="18:05:00" />5 пара: 16:30:00 - 18:05:00</label>
                        <label><input type="checkbox" name="times" data-tbeg="18:15:00" data-tend="19:50:00" />6 пара: 18:15:00 - 19:50:00</label>
                        <label><input type="checkbox" name="times" data-tbeg="00:00:00" data-tend="23:59:59" />Весь день</label>
                        <label>
                            <input type="checkbox" name="times" data-tbeg="" data-tend=""
                                className="custom-time"
                                onChange={() => {
                                    document.querySelectorAll('.time-input').forEach(elem => {
                                        elem.disabled = !elem.disabled;
                                    })
                                }}
                            />
                            <span>с</span>
                            <input type="time" disabled className="time-input"
                                onChange={e => document.querySelector('.custom-time').dataset.tbeg = e.currentTarget.value + ':00'}
                            />
                            <span>до</span>
                            <input type="time" disabled className="time-input"
                                onChange={e => document.querySelector('.custom-time').dataset.tend = e.currentTarget.value + ':00'}
                            />
                        </label>
                    </div>

                    <div className="actions-switch">
                        <div onClick={() => setIsCreate(true)} className={`${isCreate && 'chosen'}`}>Новое расписание</div>
                        <div onClick={() => setIsCreate(false)} className={`${!isCreate && 'chosen'}`}>Добавить к существующему расписанию</div>
                    </div>

                    {isCreate && <div className="action-create">
                        <div className="schedule-duration">
                            <div className="subblock-title">Длительность</div>
                            <input type="number" name="duration" />
                            <span>дн.</span>

                        </div>

                        <div className="schedule-description">
                            <div className="subblock-title">Описание</div>
                            <textarea name="description" placeholder="Введите описание..." ></textarea>
                        </div>
                    </div>}

                    {!isCreate && <div className="action-add">
                        <ul>
                            <li><div>Продолжительность</div><div>Описание</div></li>
                            {table.map((element, i) =>
                                <li
                                    key={i}
                                    data-id={element.id}
                                    onClick={() => setChosenSchedule(element.id)}
                                    className={`${chosenSchedule === element.id && 'active'}`}
                                >
                                    <div>{element.duration}</div>
                                    <div>{element.desc}</div>
                                </li>
                            )}
                        </ul>
                    </div>}
                </div>
                {<div className="create-button">
                    <button type="submit" >{isCreate ? 'Создать' : 'Добавить'}</button>
                    {showButtonNote && <div className="tooltip tooltip-top">
                        Заполните корректно <br /> все поля!
                    </div>}
                </div>}
            </form>

            <div className="table-container">
                <div
                    className="table-header"

                >
                    <span>Таблица расписаний</span>
                    <input type="text" value={searchFilter} placeholder="Поиск" onChange={e => setSearchFilter(e.currentTarget.value)} />
                    <img src={`http://${process.env.REACT_APP_URL}/static/ar-r.svg`} alt="" className={closed === '-closed' ? '' : 'up'} onClick={() => setClosed(closed === '-closed' ? '' : '-closed')} />
                </div>

                <div className={`table-body ${closed}`}>
                    <table className="tableView">
                        <thead>
                            <tr>
                                <th>Продолжительность</th>
                                <th>Описание</th>
                            </tr>
                        </thead>
                        <tbody>
                            {filteredTable.map((element, i) =>
                                <tr key={i} className="table__row">
                                    <td className="row__item">{element.duration}</td>
                                    <td className="row__item">{element.desc}</td>
                                </tr>
                            )}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
};