import React, { useState, useEffect } from 'react';
import './GeneralTable.scss';
import api from '../../api';

export default function GeneralTable() {
    let [table, setTable] = useState([]);
    let [searchFilter, setSearchFilter] = useState('');

    useEffect(() => {
        api.getAllData().then(res => setTable(res.data)).catch(console.log);
    }, []);

    const filteredTable = table.length === 0 ? table : table.filter(item => item.gtitle.toLowerCase().includes(searchFilter.toLowerCase()) || item.ptitle.toLowerCase().includes(searchFilter.toLowerCase()));

    return (
        <div className="GeneralTable">

            <div className="table-container">
                <div className="table-header" >
                    <span>Таблица связей групп и расписаний</span>
                    <input type="text" value={searchFilter} placeholder="Поиск" onChange={e => setSearchFilter(e.currentTarget.value)} />
                </div>

                <div className={`table-body`}>
                    <table className="tableView">
                        <thead>
                            <tr>
                                <th>Название группы</th>
                                <th>Описание группы</th>
                                <th>Описание расписания</th>
                                <th>Длительность расписания</th>
                                <th>День недели</th>
                                <th>Время начала и конца</th>
                                <th>Название пакета</th>
                            </tr>
                        </thead>
                        <tbody>
                            {filteredTable.map((element, i) =>
                                <tr key={i} className="table__row">
                                    <td className="row__item">{element.gtitle}</td>
                                    <td className="row__item">{element.gdesc}</td>
                                    <td className="row__item">{element.sdesc}</td>
                                    <td className="row__item">{element.duration ? `${element.duration} дней` : ''}</td>
                                    <td className="row__item">{element.weekday}</td>
                                    <td className="row__item">{element.tbeg && element.tend ? `с ${element.tbeg} по ${element.tend}` : ''}</td>
                                    <td className="row__item">{element.ptitle}</td>
                                </tr>
                            )}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
}
