import Axios from "axios";
import JwtDecode from 'jwt-decode';
import { reactLocalStorage } from 'reactjs-localstorage';

const URL = process.env.REACT_APP_URL;

const refreshTokens = (request) => new Promise((resolve, reject) => {
    Axios({
        method: 'POST',
        url: `http://${URL}/auth/refresh`,
        data: { refreshToken: reactLocalStorage.get('refreshToken') }
    })
        .then(res => {
            const data = res.data;
            const decoded = JwtDecode(data.accessToken);

            reactLocalStorage.set('accessToken', data.accessToken);
            reactLocalStorage.set('refreshToken', data.refreshToken);
            reactLocalStorage.set('expiresIn', data.expiresIn);
            reactLocalStorage.set('user', decoded.login);

            if (request) {
                resolve(Axios({
                    ...request,
                    headers: { 'Authorization': `Bearer ${reactLocalStorage.get('accessToken')}` }
                }))
            }
            else resolve();
        })
        .catch(err => {
            reactLocalStorage.clear();
            reject();
        });
})

const api = {
    getUsers: () => Axios.get(`http://${URL}/users`),

    getGroups: () => Axios.get(`http://${URL}/groups`),

    getPackages: () => Axios.get(`http://${URL}/packages`),

    getCourses: () => Axios.get(`http://${URL}/courses`),

    getWorks: () => Axios.get(`http://${URL}/works`),

    getSubs: () => Axios.get(`http://${URL}/subs`),

    getSubscriptions: () => Axios.get(`http://${URL}/subscriptions`),

    getSchedules: () => Axios.get(`http://${URL}/schedules`),

    getUsersBind: () => Axios.get(`http://${URL}/users_bind`),

    getCoursesPack: () => Axios.get(`http://${URL}/courses_pack`),

    getGroupsSubscriptions: () => Axios.get(`http://${URL}/groups_subscriptions`),

    getAllData: () => Axios.get(`http://${URL}/allData`),

    // ----------------------------------------------------------------------

    bindUserGroup: (users, groups) => {
        const request = {
            method: 'POST',
            url: `http://${URL}/bindUserGroup`,
            data: { users, groups },
            headers: { 'Authorization': `Bearer ${reactLocalStorage.get('accessToken')}` }
        }
        return Axios(request).catch(() => refreshTokens(request));
    },

    bindPackCourses: (packageId, courseId, excluded_works) => {
        const request = {
            method: 'POST',
            url: `http://${URL}/bindPackCourses`,
            data: { packageId, courseId, excluded_works },
            headers: {
                'Authorization': `Bearer ${reactLocalStorage.get('accessToken')}`
            }
        };
        return Axios(request).catch(() => refreshTokens(request));
    },

    bindGroupSub: (groupId, subId, date) => {
        const request = {
            method: 'POST',
            url: `http://${URL}/bindGroupSub`,
            data: { groupId, subId, date },
            headers: {
                'Authorization': `Bearer ${reactLocalStorage.get('accessToken')}`
            }
        };
        return Axios(request).catch(() => refreshTokens(request));
    },

    bindSchedulesPack: (packId, scheduleId, desc) => {
        const request = {
            method: 'POST',
            url: `http://${URL}/bindSchedulesPack`,
            data: { packId, scheduleId, desc },
            headers: {
                'Authorization': `Bearer ${reactLocalStorage.get('accessToken')}`
            }
        };
        return Axios(request).catch(() => refreshTokens(request));
    },

    addPackage: (pack) => {
        const request = {
            method: 'POST',
            url: `http://${URL}/packages/add`,
            data: { pack },
            headers: {
                'Authorization': `Bearer ${reactLocalStorage.get('accessToken')}`
            }
        };
        return Axios(request).catch(() => refreshTokens(request));
    },

    addSchedule: (schedule) => {
        const request = {
            method: 'POST',
            url: `http://${URL}/schedules/add`,
            data: { schedule },
            headers: {
                'Authorization': `Bearer ${reactLocalStorage.get('accessToken')}`
            }
        };
        return Axios(request).catch(() => refreshTokens(request));
    },

    updateSchedule: (schedule) => {
        let request = {
            method: 'POST',
            url: `http://${URL}/schedules/update`,
            data: { schedule },
            headers: {
                'Authorization': `Bearer ${reactLocalStorage.get('accessToken')}`
            }
        };
        return Axios(request).catch(() => refreshTokens(request))
    },

    // ----------------------------------------------------------------------

    login: (login, password) => Axios({
        method: 'POST',
        url: `http://${URL}/auth/login`,
        data: { login, password }
    })
        .then(res => {
            const data = res.data;
            const decoded = JwtDecode(data.accessToken);

            reactLocalStorage.set('accessToken', data.accessToken);
            reactLocalStorage.set('refreshToken', data.refreshToken);
            reactLocalStorage.set('expiresIn', data.expiresIn);
            reactLocalStorage.set('user', decoded.login);

            return true;
        })
        .catch(err => err),

    isAuthed: () => Axios({
        method: 'GET',
        url: `http://${URL}/auth/is-authed`,
        headers: {
            'Authorization': `Bearer ${reactLocalStorage.get('accessToken')}`
        }
    })
        .catch(() => refreshTokens())
}

export default api;