import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './components/App/App.jsx';

import { BrowserRouter } from 'react-router-dom';

ReactDOM.render(
    <BrowserRouter basename={`${process.env.REACT_APP_BASENAME}`}>
        <App />
    </BrowserRouter>,
    document.getElementById('root')
);
